FROM python:alpine
ADD . /srv/
WORKDIR /srv/
RUN pip install --no-cache-dir -r requirements.txt && \
    apk add bash curl wget
CMD [ "bash", "./run.sh"]
#CMD [ "python", "./app.py" ]